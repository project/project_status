This test suite checks that the output of project status and of pm-list are the
same when both are asked to list both enabled and unused modules.  Run this
from your platform's sites/ directory.  If all is working correctly you should
see no output from this script other than a list of your sites.
